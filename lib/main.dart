import 'package:flutter/material.dart';
import 'package:codeunion_task/core/injections/sl.dart';
import 'core/_index.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setUpServiceLocator();
  runApp(const AppWidget());
}
