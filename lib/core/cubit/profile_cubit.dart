import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:codeunion_task/features/auth/data/models/_index.dart';

class ProfileCubit extends Cubit<ProfileDataModel?> {
  ProfileCubit() : super(null);

  void setProfileAppData(ProfileDataModel e) {
    emit(e);
  }
}
