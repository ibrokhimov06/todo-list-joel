part of '../_index.dart';

extension UIThemeExtension on BuildContext {
  TextStyle? get extraBold18 => TextThemeExtension.extraBold18;
  TextStyle? get extraBold22 => TextThemeExtension.extraBold22;
  TextStyle? get extraBold32 => TextThemeExtension.extraBold32;

  TextStyle? get semiBold48 => TextThemeExtension.semiBold48;
  TextStyle? get semiBold40 => TextThemeExtension.semiBold40;
  TextStyle? get semiBold32 => TextThemeExtension.semiBold32;
  TextStyle? get semiBold30 => TextThemeExtension.semiBold30;
  TextStyle? get semiBold18 => TextThemeExtension.semiBold18;
  TextStyle? get semiBold14 => TextThemeExtension.semiBold14;

  TextStyle? get regular33 => TextThemeExtension.regular33;
  TextStyle? get regular22 => TextThemeExtension.regular22;

  TextStyle? get regular14 => TextThemeExtension.regular14;

  TextStyle? get regular18 => TextThemeExtension.regular18;
  TextStyle? get regular20 => TextThemeExtension.regular20;

  TextStyle? get light18 => TextThemeExtension.light18;

  double get screenHeight => MediaQuery.of(this).size.height;
  double get screenWidth => MediaQuery.of(this).size.width;

  Color get primary => AppColors.primary;
  Color get black => AppColors.black;
  Color get white => AppColors.white;
  Color get greyLight => AppColors.greyLight;
  Color get grey => AppColors.grey;
  Color get danger => AppColors.danger;

  double get spaceXL => AppSizes.spaceXL;
  double get spaceL => AppSizes.spaceL;
  double get spaceM => AppSizes.spaceM;
  double get spaceS => AppSizes.spaceS;
}

class TextThemeExtension {
  // EXTRA BOLD
  static const extraBold18 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w900,
    color: AppColors.black,
  );
  static const extraBold22 = TextStyle(
    fontSize: 22,
    fontWeight: FontWeight.w900,
    color: AppColors.black,
  );
  static const extraBold32 = TextStyle(
    fontSize: 32,
    fontWeight: FontWeight.w900,
    color: AppColors.black,
  );
  // SEMIBOLD
  static const semiBold48 = TextStyle(
    fontSize: 48,
    fontWeight: FontWeight.w600,
    color: AppColors.black,
  );
  static const semiBold40 = TextStyle(
    fontSize: 40,
    fontWeight: FontWeight.w600,
    color: AppColors.black,
  );

  static const semiBold32 = TextStyle(
    fontSize: 32,
    fontWeight: FontWeight.w600,
    color: AppColors.black,
  );

  static const semiBold30 = TextStyle(
    fontSize: 30,
    fontWeight: FontWeight.w600,
    color: AppColors.black,
  );

  static const semiBold18 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w600,
    color: AppColors.black,
  );

  static const semiBold14 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w600,
    color: AppColors.black,
  );

  // REGULAR
  static const regular33 = TextStyle(
    fontSize: 33,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );

  static const regular22 = TextStyle(
    fontSize: 22,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );

  static const regular20 = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );

  static const regular18 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );

  static const regular14 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );

  // LIGHT
  static const light18 = TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w300,
    color: AppColors.black,
  );
}
