part of '../_index.dart';

class AppColors {
  static const Color primary = Color(0xFF4631D2);
  static const Color white = Color(0xFFFEFEFE);
  static const Color grey = Color(0xFF929292);
  static const Color greyLight = Color(0xFFF3F4F6);
  static const Color black = Color(0xFF000000);
  static const Color danger = Color(0XFFF0142F);
}

class AppSizes {
  static const double spaceXL = 40;
  static const double spaceL = 30;
  static const double spaceM = 15;
  static const double spaceS = 5;
}
