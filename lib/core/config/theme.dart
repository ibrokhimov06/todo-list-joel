part of '../_index.dart';

class AppTheme {
  static const CupertinoThemeData theme = CupertinoThemeData(
    primaryColor: AppColors.primary,
    barBackgroundColor: AppColors.white,
    scaffoldBackgroundColor: AppColors.greyLight,
  );
}
