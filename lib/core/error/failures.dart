part of '../_index.dart';

abstract class Failure extends Equatable {
  final String message;

  const Failure([this.message = '']) : super();
}

class SiginFailure extends Failure {
  final String message;

  const SiginFailure(this.message);

  @override
  List<Object?> get props => [message];
}

class GetProfileFailure extends Failure {
  final String message;

  const GetProfileFailure(this.message);

  @override
  List<Object?> get props => [message];
}
