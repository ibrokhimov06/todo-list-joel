part of '../_index.dart';

class Endpoints {
  Endpoints._();

  static const String baseUrl = "http://45.10.110.181:8080/api/v1";

  static const String login = "/auth/login";
  static const String profile = "/auth/login/profile";

  static const int receiveTimeout = 100000;
  static const int connectionTimeout = 100000;
}
