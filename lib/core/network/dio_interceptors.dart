part of '../_index.dart';

class AppInterceptors extends Interceptor {
  final Dio? dio;
  final AuthLocalDataSource authLocalDataSource;

  AppInterceptors({required this.dio, required this.authLocalDataSource});

  @override
  void onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    final token = authLocalDataSource.getToken();

    if (token != null) {
      options.headers['Authorization'] = 'Bearer $token';
    }

    return handler.next(options);
  }
}
