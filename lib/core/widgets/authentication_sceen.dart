part of '../_index.dart';

class AuthenticationScreen extends StatefulWidget {
  const AuthenticationScreen({super.key});
  static const routeName = "authentication";

  @override
  State<AuthenticationScreen> createState() => _AuthenticationScreenState();
}

class _AuthenticationScreenState extends State<AuthenticationScreen> {
  @override
  void initState() {
    BlocProvider.of<AuthBloc>(context).add(Authenticate());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
        builder: (_, state) {
          final fetchingProfile = state is GetProfileLoading;
          return CupertinoPageScaffold(
            child: Center(
                child: Text(fetchingProfile
                    ? "Fetching profile..."
                    : "Authenticating...")),
          );
        },
        listener: _profileBlocListener);
  }

  void _profileBlocListener(BuildContext _, AuthState state) {
    print(state);
    if (state is Authenticated) {
      BlocProvider.of<AuthBloc>(context).add(GetProfile());
    } else if (state is UnAuthenticated) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        SigninScreen.routeName,
        (route) => false,
      );
    } else if (state is GetProfileSuccess) {
      context.read<ProfileCubit>().setProfileAppData(state.data);
      Navigator.pushNamedAndRemoveUntil(
        context,
        AppLayout.routeName,
        (route) => false,
      );
    } else if (state is AuthenticationError) {
      showAlertDialog(context, state.message);
    }
  }
}
