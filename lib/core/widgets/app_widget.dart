part of '../_index.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (BuildContext context) => ProfileCubit()),
        BlocProvider(create: (BuildContext context) => sl<AuthBloc>()),
      ],
      child: const CupertinoApp(
        onGenerateRoute: RouteGenerator.generateRoute,
        home: AuthenticationScreen(),
        debugShowCheckedModeBanner: false,
        theme: AppTheme.theme,
      ),
    );
  }
}
