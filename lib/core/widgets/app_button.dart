part of '../_index.dart';

class AppButton extends StatelessWidget {
  const AppButton({super.key, required this.onPressed, required this.text});
  final void Function() onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 64,
      width: double.infinity,
      child: CupertinoButton.filled(
        onPressed: onPressed,
        child: Text(text),
      ),
    );
  }
}
