part of '../_index.dart';

class AppTextField extends StatelessWidget {
  final String placeholder;
  final bool required;
  final List validator;
  final TextEditingController controller;
  final bool hasBottomBorder;
  final bool email;

  const AppTextField({
    Key? key,
    this.required = true,
    this.email = false,
    required this.controller,
    required this.placeholder,
    this.hasBottomBorder = true,
    this.validator = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(
        bottom: hasBottomBorder
            ? const BorderSide(color: Color(0xFFC3C3C3), width: 0.5)
            : BorderSide.none,
      )),
      child: CupertinoTextFormFieldRow(
        validator: (String? value) {
          final bool emailValid = RegExp(
                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(value!);
          if (value.isEmpty && required) {
            return 'This field is required!';
          }
          if (email && !emailValid) {
            return "Email must be a valid!";
          }

          return null;
        },
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 0),
        placeholder: placeholder,
        controller: controller,
        placeholderStyle: const TextStyle(
          fontSize: 16,
          color: Color(0xFFC3C3C3),
        ),
        decoration: BoxDecoration(
          color: context.white,
          border: const Border(),
        ),
      ),
    );
  }
}
