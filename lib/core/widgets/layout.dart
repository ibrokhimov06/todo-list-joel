part of '../_index.dart';

class AppLayout extends StatefulWidget {
  const AppLayout({super.key});
  static const routeName = 'layout';

  @override
  State<AppLayout> createState() => _AppLayoutState();
}

class _AppLayoutState extends State<AppLayout> {
  int currentIndex = 3;
  static const pages = [
    NotFound(),
    NotFound(),
    NotFound(),
    ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: _authListener,
      child: CupertinoTabScaffold(
        backgroundColor: context.white,
        tabBar: CupertinoTabBar(
          currentIndex: currentIndex,
          onTap: onTap,
          border: null,
          inactiveColor: context.black,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.cube_box),
              label: 'Favorites',
            ),
            BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.zoom_out),
              label: 'Recents',
            ),
            BottomNavigationBarItem(
              icon: Icon(CupertinoIcons.pencil_outline),
              label: 'Contacts',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.account_circle_outlined),
              label: 'Профиль',
            ),
          ],
        ),
        tabBuilder: (BuildContext context, int index) {
          return CupertinoTabView(
            builder: (_) => pages[currentIndex],
          );
        },
      ),
    );
  }

  void _authListener(BuildContext _, AuthState state) {
    if (state is LogoutSuccess) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        SigninScreen.routeName,
        (route) => false,
      );
    }
  }

  void onTap(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}
