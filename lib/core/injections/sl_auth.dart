import 'package:codeunion_task/features/auth/_index.dart';

import 'sl.dart';

Future setUpAuthSl() async {
  // Bloc
  sl.registerFactory<AuthBloc>(
    () => AuthBloc(
      login: sl(),
      saveToken: sl(),
      removeToken: sl(),
      getToken: sl(),
      getProfile: sl(),
    ),
  );

  // Repository
  sl.registerLazySingleton<AuthRepository>(
    () => AuthRepositoryImpl(
      remoteDataSource: sl(),
      localDataSource: sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton(() => LoginUsecase(sl()));
  sl.registerLazySingleton(() => GetTokenUsecase(sl()));
  sl.registerLazySingleton(() => RemoveTokenUsecase(sl()));
  sl.registerLazySingleton(() => SaveTokenUsecase(sl()));
  sl.registerLazySingleton(() => GetProfileUsecase(sl()));

  // Data sources
  sl.registerLazySingleton<AuthRemoteDataSource>(
    () => AuthRemoteDataSourceImpl(dio: sl()),
  );
  sl.registerLazySingleton<AuthLocalDataSource>(
    () => AuthLocalDataSourceImpl(prefs: sl()),
  );
}
