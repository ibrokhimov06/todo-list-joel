import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:codeunion_task/core/_index.dart';
import 'package:codeunion_task/core/cubit/profile_cubit.dart';

import 'sl_auth.dart';

final sl = GetIt.instance;

Future setUpServiceLocator() async {
  await setUpAuthSl();

  // Bloc/Cubit
  sl.registerFactory<ProfileCubit>(() => ProfileCubit());

  // External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton<DioClient>(() => DioClient(Dio()));
}
