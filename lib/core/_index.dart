import 'package:dartz/dartz.dart' as dartz;
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:codeunion_task/core/cubit/profile_cubit.dart';
import 'package:codeunion_task/core/injections/sl.dart';
import 'package:codeunion_task/features/auth/_index.dart';
import 'package:dio/dio.dart';
import 'package:codeunion_task/features/auth/data/models/_index.dart';
import 'package:codeunion_task/features/profile/presentation/pages/profile_screen.dart';
import 'package:codeunion_task/routes/routes.dart';

part 'usecase.dart';
// config
part './config/theme.dart';
part './config/constants.dart';
part './config/extensions.dart';
part './utils/helpers.dart';

// error
part './error/failures.dart';

// network
part './network/dio_client.dart';
part './network/endpoints.dart';
part './network/dio_interceptors.dart';
part './network/dio_exceptions.dart';

// widgets
part './widgets/app_container.dart';
part './widgets/app_widget.dart';
part './widgets/layout.dart';
part '../features/auth/presentation/pages/signin_screen.dart';
part './widgets/authentication_sceen.dart';
part './widgets/notfound.dart';
part './widgets/textfield.dart';
part './widgets/app_button.dart';
