part of '../_index.dart';

void showAlertDialog(BuildContext context, String message) {
  showCupertinoModalPopup<void>(
    context: context,
    builder: (BuildContext context) => CupertinoAlertDialog(
      title: const Text('Error'),
      content: Text(message),
    ),
  );
}
