import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:codeunion_task/core/_index.dart';
import 'package:codeunion_task/features/profile/presentation/pages/profile_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // var arguments = settings.arguments as dynamic;
    switch (settings.name) {
      // AUTH ROUTES
      case AuthenticationScreen.routeName:
        return PageTransition(
          child: const AuthenticationScreen(),
          type: PageTransitionType.fade,
          settings: settings,
        );
      case SigninScreen.routeName:
        return PageTransition(
          child: const SigninScreen(),
          type: PageTransitionType.fade,
          settings: settings,
        );
      case AppLayout.routeName:
        return PageTransition(
          child: const AppLayout(),
          type: PageTransitionType.fade,
          settings: settings,
        );
      case ProfileScreen.routeName:
        return PageTransition(
          child: const ProfileScreen(),
          type: PageTransitionType.fade,
          settings: settings,
        );
    }
    return PageTransition(
      child: const NotFound(),
      type: PageTransitionType.fade,
      settings: settings,
    );
  }
}
