part of 'package:codeunion_task/features/auth/_index.dart';

abstract class AuthLocalDataSource {
  String? getToken();
  Future<void> saveToken(String token);
  void removeToken();
}

class AuthLocalDataSourceImpl implements AuthLocalDataSource {
  final SharedPreferences prefs;
  static const cachedToken = 'cachedToken';

  const AuthLocalDataSourceImpl({required this.prefs});

  @override
  String? getToken() {
    try {
      String? token = prefs.getString(cachedToken);

      return token == null ? null : jsonDecode(token);
    } catch (error) {
      rethrow;
    }
  }

  @override
  Future<void> saveToken(token) async {
    try {
      await prefs.setString(cachedToken, jsonEncode(token));
    } catch (error) {
      rethrow;
    }
  }

  @override
  void removeToken() {
    try {
      prefs.remove(cachedToken);
    } catch (error) {
      rethrow;
    }
  }
}
