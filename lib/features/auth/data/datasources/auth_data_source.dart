part of 'package:codeunion_task/features/auth/_index.dart';

abstract class AuthRemoteDataSource {
  Future<String> login(LoginDataModel loginData);
  Future<ProfileDataModel> getProfile();
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  final DioClient dio;

  const AuthRemoteDataSourceImpl({required this.dio});

  @override
  Future<String> login(data) async {
    try {
      final response = await dio.post(Endpoints.login, data: data.toJson());

      return response.data['tokens']['accessToken'];
    } on DioError catch (dioError) {
      throw DioExceptions.fromDioError(dioError);
    }
  }

  @override
  Future<ProfileDataModel> getProfile() async {
    try {
      final response = await dio.get(Endpoints.profile);

      return ProfileDataModel.fromJson(response.data);
    } on DioError catch (dioError) {
      throw DioExceptions.fromDioError(dioError);
    }
  }
}
