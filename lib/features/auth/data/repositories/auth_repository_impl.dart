part of 'package:codeunion_task/features/auth/_index.dart';

class AuthRepositoryImpl implements AuthRepository {
  AuthRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
  });

  final AuthRemoteDataSource remoteDataSource;
  final AuthLocalDataSource localDataSource;

  @override
  Future<Either<Failure, String>> login(data) async {
    try {
      final profile = await remoteDataSource.login(data);
      return Right(profile);
    } catch (e) {
      return Left(SiginFailure(e.toString()));
    }
  }

  @override
  Future<Either<Failure, ProfileDataModel>> getProfile() async {
    try {
      final profile = await remoteDataSource.getProfile();
      return Right(profile);
    } catch (e) {
      return Left(GetProfileFailure(e.toString()));
    }
  }

  @override
  Either<Failure, String?> getToken() {
    try {
      final profile = localDataSource.getToken();
      return Right(profile);
    } catch (e) {
      return Left(SiginFailure(e.toString()));
    }
  }

  @override
  Future<Either<Failure, void>> saveToken(data) async {
    try {
      await localDataSource.saveToken(data);
      return const Right(null);
    } catch (e) {
      return Left(SiginFailure(e.toString()));
    }
  }

  @override
  Either<Failure, void> removeToken() {
    try {
      localDataSource.removeToken();
      return const Right(null);
    } catch (e) {
      return Left(SiginFailure(e.toString()));
    }
  }
}
