// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'profile_data_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProfileDataModel _$ProfileDataModelFromJson(Map<String, dynamic> json) {
  return _ProfileDataModel.fromJson(json);
}

/// @nodoc
mixin _$ProfileDataModel {
  int get id => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get nickname => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProfileDataModelCopyWith<ProfileDataModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileDataModelCopyWith<$Res> {
  factory $ProfileDataModelCopyWith(
          ProfileDataModel value, $Res Function(ProfileDataModel) then) =
      _$ProfileDataModelCopyWithImpl<$Res, ProfileDataModel>;
  @useResult
  $Res call({int id, String email, String nickname});
}

/// @nodoc
class _$ProfileDataModelCopyWithImpl<$Res, $Val extends ProfileDataModel>
    implements $ProfileDataModelCopyWith<$Res> {
  _$ProfileDataModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? email = null,
    Object? nickname = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      nickname: null == nickname
          ? _value.nickname
          : nickname // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ProfileDataModelCopyWith<$Res>
    implements $ProfileDataModelCopyWith<$Res> {
  factory _$$_ProfileDataModelCopyWith(
          _$_ProfileDataModel value, $Res Function(_$_ProfileDataModel) then) =
      __$$_ProfileDataModelCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String email, String nickname});
}

/// @nodoc
class __$$_ProfileDataModelCopyWithImpl<$Res>
    extends _$ProfileDataModelCopyWithImpl<$Res, _$_ProfileDataModel>
    implements _$$_ProfileDataModelCopyWith<$Res> {
  __$$_ProfileDataModelCopyWithImpl(
      _$_ProfileDataModel _value, $Res Function(_$_ProfileDataModel) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? email = null,
    Object? nickname = null,
  }) {
    return _then(_$_ProfileDataModel(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      nickname: null == nickname
          ? _value.nickname
          : nickname // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ProfileDataModel implements _ProfileDataModel {
  _$_ProfileDataModel(
      {required this.id, required this.email, required this.nickname});

  factory _$_ProfileDataModel.fromJson(Map<String, dynamic> json) =>
      _$$_ProfileDataModelFromJson(json);

  @override
  final int id;
  @override
  final String email;
  @override
  final String nickname;

  @override
  String toString() {
    return 'ProfileDataModel(id: $id, email: $email, nickname: $nickname)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProfileDataModel &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.nickname, nickname) ||
                other.nickname == nickname));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, email, nickname);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ProfileDataModelCopyWith<_$_ProfileDataModel> get copyWith =>
      __$$_ProfileDataModelCopyWithImpl<_$_ProfileDataModel>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProfileDataModelToJson(
      this,
    );
  }
}

abstract class _ProfileDataModel implements ProfileDataModel {
  factory _ProfileDataModel(
      {required final int id,
      required final String email,
      required final String nickname}) = _$_ProfileDataModel;

  factory _ProfileDataModel.fromJson(Map<String, dynamic> json) =
      _$_ProfileDataModel.fromJson;

  @override
  int get id;
  @override
  String get email;
  @override
  String get nickname;
  @override
  @JsonKey(ignore: true)
  _$$_ProfileDataModelCopyWith<_$_ProfileDataModel> get copyWith =>
      throw _privateConstructorUsedError;
}
