import 'package:freezed_annotation/freezed_annotation.dart';
part 'login_data_model.freezed.dart';
part 'login_data_model.g.dart';

@freezed
abstract class LoginDataModel with _$LoginDataModel {
  factory LoginDataModel({
    required String email,
    required String password,
  }) = _LoginDataModel;
  factory LoginDataModel.fromJson(Map<String, dynamic> json) =>
      _$LoginDataModelFromJson(json);
}
