import 'package:freezed_annotation/freezed_annotation.dart';
part 'profile_data_model.freezed.dart';
part 'profile_data_model.g.dart';

@freezed
abstract class ProfileDataModel with _$ProfileDataModel {
  factory ProfileDataModel({
    required int id,
    required String email,
    required String nickname,
  }) = _ProfileDataModel;
  factory ProfileDataModel.fromJson(Map<String, dynamic> json) =>
      _$ProfileDataModelFromJson(json);
}
