// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ProfileDataModel _$$_ProfileDataModelFromJson(Map<String, dynamic> json) =>
    _$_ProfileDataModel(
      id: json['id'] as int,
      email: json['email'] as String,
      nickname: json['nickname'] as String,
    );

Map<String, dynamic> _$$_ProfileDataModelToJson(_$_ProfileDataModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'nickname': instance.nickname,
    };
