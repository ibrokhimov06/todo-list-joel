import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:codeunion_task/core/_index.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import './data/models/_index.dart';

// data
part './data/datasources/auth_data_source.dart';
part './data/datasources/auth_local_data_source.dart';
part './data/repositories/auth_repository_impl.dart';

// domain
part './domain/repositories/auth_repository.dart';
part './domain/usecases/login_usecase.dart';
part 'domain/usecases/get_token_usecase.dart';
part 'domain/usecases/remove_token_usecase.dart';
part 'domain/usecases/save_token_usecase.dart';
part 'domain/usecases/get_profile_usecase.dart';

// presentation
part './presentation/bloc/auth_bloc.dart';
part './presentation/bloc/auth_event.dart';
part './presentation/bloc/auth_state.dart';
