part of 'package:codeunion_task/features/auth/_index.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthStateInitial extends AuthState {}

// AUTHENTICATION
class Authenticated extends AuthState {}

class AuthenticationLoading extends AuthState {}

class UnAuthenticated extends AuthState {}

class AuthenticationError extends AuthState {
  final String message;

  const AuthenticationError({required this.message});

  @override
  List<Object> get props => [message];
}

// LOGIN
class LoginLoading extends AuthState {}

class LoginSuccess extends AuthState {}

class LogoutSuccess extends AuthState {}

class LoginError extends AuthState {
  final Failure failure;

  const LoginError({required this.failure});

  @override
  List<Object> get props => [failure];
}

// Profile
class GetProfileLoading extends AuthState {}

class GetProfileSuccess extends AuthState {
  final ProfileDataModel data;

  const GetProfileSuccess(this.data);

  @override
  List<Object> get props => [data];
}

class GetProfileError extends AuthState {
  final String message;

  const GetProfileError({required this.message});

  @override
  List<Object> get props => [message];
}
