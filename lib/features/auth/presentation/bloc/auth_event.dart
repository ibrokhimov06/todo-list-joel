part of 'package:codeunion_task/features/auth/_index.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class Login extends AuthEvent {
  final LoginDataModel loginData;

  const Login(this.loginData);

  @override
  List<Object> get props => [loginData];
}

class Logout extends AuthEvent {}

class Authenticate extends AuthEvent {}

class GetProfile extends AuthEvent {}
