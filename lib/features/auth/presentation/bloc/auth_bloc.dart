part of 'package:codeunion_task/features/auth/_index.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc({
    required this.login,
    required this.getToken,
    required this.saveToken,
    required this.removeToken,
    required this.getProfile,
  }) : super(AuthStateInitial()) {
    on<Login>(_login);
    on<Authenticate>(_authenticate);
    on<Logout>(_logout);
    on<GetProfile>(_getProfile);
  }

  final LoginUsecase login;
  final GetTokenUsecase getToken;
  final GetProfileUsecase getProfile;
  final RemoveTokenUsecase removeToken;
  final SaveTokenUsecase saveToken;

  _authenticate(
    Authenticate event,
    Emitter<AuthState> emit,
  ) async {
    await Future.delayed(const Duration(milliseconds: 650));

    emit(AuthenticationLoading());

    final failureOrSuccess = await getToken(NoParams());

    emit(
      failureOrSuccess.fold(
        (l) => AuthenticationError(message: l.message),
        (token) => token == null ? UnAuthenticated() : Authenticated(),
      ),
    );
  }

  _getProfile(
    GetProfile event,
    Emitter<AuthState> emit,
  ) async {
    emit(GetProfileLoading());

    final failureOrSuccess = await getProfile(NoParams());

    emit(
      failureOrSuccess.fold(
        (l) => GetProfileError(message: l.message),
        (data) => GetProfileSuccess(data),
      ),
    );
  }

  Future<void> _login(
    Login event,
    Emitter<AuthState> emit,
  ) async {
    emit(LoginLoading());
    final failureOrLoginResponse = await login(event.loginData);

    failureOrLoginResponse.fold(
      (message) => emit(LoginError(failure: message)),
      (loginResponse) async {
        emit(LoginSuccess());
        await saveToken(loginResponse);
      },
    );
  }

  Future<void> _logout(
    Logout event,
    Emitter<AuthState> emit,
  ) async {
    await removeToken(NoParams());
    emit(LogoutSuccess());
  }
}
