part of '../../../../core/_index.dart';

class SigninScreen extends StatefulWidget {
  const SigninScreen({super.key});
  static const routeName = "signin";

  @override
  State<SigninScreen> createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  final ctrlEmail = TextEditingController();
  final ctrlPassword = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Авторизация'),
        border: null,
      ),
      child: BlocListener<AuthBloc, AuthState>(
        listener: _authBlocListener,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              color: context.white,
              child: AppContainer(
                child: Form(
                  key: _formKey,
                  autovalidateMode: AutovalidateMode.always,
                  child: Column(
                    children: [
                      AppTextField(
                        controller: ctrlEmail,
                        placeholder: "Email",
                        email: true,
                      ),
                      AppTextField(
                        controller: ctrlPassword,
                        placeholder: "Password",
                        hasBottomBorder: false,
                      )
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(height: context.spaceL),
            AppContainer(
                child: Column(
              children: [
                BlocBuilder<AuthBloc, AuthState>(builder: (context, state) {
                  return AppButton(
                    onPressed: _onSubmit,
                    text: state is LoginLoading ? "Loading..." : "Войти",
                  );
                }),
                SizedBox(height: context.spaceM),
                AppButton(onPressed: () {}, text: "Зарегистрироваться")
              ],
            ))
          ],
        ),
      ),
    );
  }

  void _onSubmit() {
    if (_formKey.currentState!.validate()) {
      final loginData = LoginDataModel(
        email: ctrlEmail.text,
        password: ctrlPassword.text,
      );
      context.read<AuthBloc>().add(Login(loginData));
    }
  }

  void _authBlocListener(BuildContext _, AuthState state) {
    if (state is LoginSuccess) {
      Navigator.pushNamedAndRemoveUntil(
        context,
        AuthenticationScreen.routeName,
        (route) => false,
      );
    } else if (state is LoginError) {
      showAlertDialog(context, state.failure.message);
    }
  }

  @override
  void dispose() {
    ctrlEmail.dispose();
    ctrlPassword.dispose();
    super.dispose();
  }
}
