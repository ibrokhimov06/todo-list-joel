part of 'package:codeunion_task/features/auth/_index.dart';

class SaveTokenUsecase extends UseCase<void, String> {
  final AuthRepository repository;

  SaveTokenUsecase(this.repository);

  @override
  Future<Either<Failure, void>> call(String params) {
    return repository.saveToken(params);
  }
}
