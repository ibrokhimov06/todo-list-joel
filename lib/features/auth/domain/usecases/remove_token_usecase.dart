part of 'package:codeunion_task/features/auth/_index.dart';

class RemoveTokenUsecase extends UseCase<void, NoParams> {
  final AuthRepository repository;

  RemoveTokenUsecase(this.repository);

  @override
  Future<Either<Failure, void>> call(NoParams params) {
    return Future.value(repository.removeToken());
  }
}
