part of 'package:codeunion_task/features/auth/_index.dart';

class LoginUsecase extends UseCase<String, LoginDataModel> {
  final AuthRepository repository;

  LoginUsecase(this.repository);

  @override
  Future<Either<Failure, String>> call(LoginDataModel params) async {
    return await repository.login(params);
  }
}
