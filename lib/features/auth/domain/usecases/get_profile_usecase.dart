part of 'package:codeunion_task/features/auth/_index.dart';

class GetProfileUsecase extends UseCase<ProfileDataModel, NoParams> {
  final AuthRepository repository;

  GetProfileUsecase(this.repository);

  @override
  Future<Either<Failure, ProfileDataModel>> call(NoParams params) {
    return Future.value(repository.getProfile());
  }
}
