part of 'package:codeunion_task/features/auth/_index.dart';

class GetTokenUsecase extends UseCase<String?, NoParams> {
  final AuthRepository repository;

  GetTokenUsecase(this.repository);

  @override
  Future<Either<Failure, String?>> call(NoParams params) {
    return Future.value(repository.getToken());
  }
}
