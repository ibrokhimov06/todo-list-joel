part of 'package:codeunion_task/features/auth/_index.dart';

abstract class AuthRepository {
  Future<Either<Failure, String>> login(LoginDataModel loginData);
  Future<Either<Failure, ProfileDataModel>> getProfile();
  Either<Failure, String?> getToken();
  Future<Either<Failure, void>> saveToken(String data);
  Either<Failure, void> removeToken();
}
