import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:codeunion_task/core/_index.dart';
import 'package:codeunion_task/core/cubit/profile_cubit.dart';
import 'package:codeunion_task/features/auth/_index.dart';
import 'package:codeunion_task/features/auth/data/models/_index.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});
  static const routeName = "profile";

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late ProfileDataModel profile;

  @override
  void initState() {
    profile = context.read<ProfileCubit>().state!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        middle: Text('Profile'),
        border: null,
      ),
      child: Column(
        children: [
          AppContainer(
            hasTopPadding: true,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.account_circle_outlined,
                    size: 80,
                    color: context.black,
                  ),
                  SizedBox(height: context.spaceM),
                  Text(profile.nickname, style: context.semiBold30),
                  SizedBox(height: context.spaceM),
                  Text(
                    profile.email,
                    style: context.regular18?.copyWith(color: context.grey),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: context.spaceL),
          SizedBox(
            width: double.infinity,
            child: CupertinoButton(
              padding: const EdgeInsets.all(0),
              borderRadius: const BorderRadius.all(Radius.zero),
              alignment: Alignment.centerLeft,
              color: context.white,
              child: AppContainer(
                hasTopPadding: true,
                hasBottomPadding: true,
                child: Text(
                  "Выйти",
                  style: context.regular18?.copyWith(color: context.danger),
                  textAlign: TextAlign.left,
                ),
              ),
              onPressed: () => context.read<AuthBloc>().add(Logout()),
            ),
          ),
        ],
      ),
    );
  }
}
